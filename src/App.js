import React from 'react'
import Matches from './Components/Matches.js'
import Player from './Components/Player.js'
import { useMyContext } from './context'
import GameOptions from './Components/GameOptions'

function App() {
  const { gameSettings, progressArray } = useMyContext();

  return (
    <div className="row gameField deep-purple lighten-5">
      <h1>Matches Picker</h1>
      { gameSettings.gameStarted ? 
      <>
        <Matches/>      
        <Player player={ progressArray.filter((element, index) => index % 2 === 0) } playerN='1st' />
        <Player player={ progressArray.filter((element, index) => index % 2 !== 0) } playerN='2nd' />
      </>
      : <GameOptions/> }
    </div>
  );
}

export default App;
