import { useMyContext } from '../context'

function GameOptions() {
  const {
    gameSettings, setGameSettings, 
    nm, setNM, 
    setProgress
  } = useMyContext()

  const [n, m] = nm

  const startGame = () => {
    // first turn by AI
    if(gameSettings.gameMode === 'AI') {
      const randomM1 = Math.floor(Math.random() * m) + 1,
        randomM2 = Math.floor(Math.random() * m) + 1
        setProgress([randomM1, randomM2, 0])
    }
    const newGameSettings = Object.assign({}, gameSettings)
    newGameSettings.gameStarted = true
    setGameSettings(newGameSettings)
  }

  const changeGameType = (newType) => {
    const newGameSettings = Object.assign({}, gameSettings)
    newGameSettings.gameType = newType
    setGameSettings(newGameSettings)
  }

  const changeGameMode = (newMode) => {
    const newGameSettings = Object.assign({}, gameSettings)
    newGameSettings.gameMode = newMode
    setGameSettings(newGameSettings)
  }

  return (
      <>
        <div className="row">
          <div className="col s6">
            <select className="browser-default" value={gameSettings.gameMode} onChange={ e => changeGameMode(e.target.value) }>
              <option value="" disabled defaultValue >Choose game mode</option>
              <option value="AI">First turn by AI</option>
              <option value="Normal">Normal</option>
            </select>          
            <label>Game Mode</label>
          </div>
          <div className="col s6">
            <select className="browser-default" value={gameSettings.gameType} onChange={ e => changeGameType(e.target.value) }>
              <option value="" disabled defaultValue>Choose game type</option>
              <option value="AI">vs AI</option>
              <option value="Normal">vs Player</option>
            </select>          
            <label>Game Type</label>
          </div>
          <div className="col s6">
            <input id='m' value={m} onChange={ e => setNM([n, parseInt(e.target.value)]) }/>
            <label htmlFor="m">Add parameter "M"</label>
          </div>
          <div className="col s6">
            <input id='n' value={n} onChange={ e => setNM([parseInt(e.target.value), m]) }/>
            <label htmlFor="n">Add parameter "N"</label>
          </div>
        </div>
        <button onClick={ startGame } className="button waves-effect waves-light btn-large">Start Game</button> 
      </>      
  );
}

export default GameOptions;