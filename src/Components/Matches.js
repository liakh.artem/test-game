import React, { useEffect, useCallback, useMemo } from 'react';
import { useMyContext } from '../context'

function Matches() {
  const {
    progressArray,
    setProgress,
    gameSettings,
    setGameSettings,
    nm,
  } = useMyContext()
  
  const [n, m] = nm
  
  const turnOfAI = useCallback(() => {
    // checks if AI turn
    if(progressArray[progressArray.length - 1] === 0 && 
      progressArray.length % 2 === 0 && 
      gameSettings.gameType === 'AI') {        
      const matches = 2 * n + 1 - progressArray.reduce((a, b) => a + b),
        secondPlayer = progressArray.filter((element, index) => index % 2 !== 0),
        secondPlayedCount = secondPlayer.reduce((a, b) => a + b)
      let randomNum = matches // if m less than matches on the table

      if(matches >= m) { // if m more than matches on the table
        randomNum = Math.floor(Math.random() * m) + 1
    
        // makes choises of AI optimal in the end of the game
        if(m % 2 !== 0) {
          if(matches === m + 1 && secondPlayedCount % 2 !== 0) {
            randomNum = m
          } else if(matches === m && secondPlayedCount % 2 === 0) {
            randomNum = m - 1
          } else if(matches === m && secondPlayedCount % 2 !== 0) {
            randomNum = m
          } 
        } 
        
        if (m % 2 === 0) {
          if(matches === m && secondPlayedCount % 2 !== 0) {
              randomNum = m - 1
          } else if(matches === m - 1 && secondPlayedCount % 2 === 0) {
            randomNum = m - 2
          } else if(matches === m - 1 && secondPlayedCount % 2 !== 0) {
            randomNum = m - 1
          } 
        }
      }

      if (m % 2 !== 0) {
        if(matches === 2 && secondPlayedCount % 2 !== 0) {
          randomNum = 1
        } else if(matches === 2 && secondPlayedCount % 2 === 0) {
          randomNum = 2
        }
      } 
      
      if (m % 2 === 0) {
        if(matches === 2 && secondPlayedCount % 2 !== 0) {
          randomNum = 1
        } else if(matches === 2 && secondPlayedCount % 2 === 0) {
          randomNum = 2
        }
      }

      // adds AI turn to progressArray
      const newProgress = [...progressArray]
      newProgress[newProgress.length - 1] = newProgress[newProgress.length - 1] + randomNum
      newProgress.push(0) // adds next turn
      setProgress(newProgress)
    }  
  }, [progressArray, gameSettings.gameType, n, m, setProgress])
  
  const checkWinner = useCallback(() => {
    // checks if all matches taken
    if(progressArray.reduce((a, b) => a + b) === 2 * n + 1) {
      // counts firstPlayer matches and check if they are even or odd
      const firstPlayer = progressArray.filter((element, index) => index % 2 === 0).reduce((a, b) => a + b)
      if(firstPlayer % 2 === 0) {
        alert('First player won!')
      } else { 
        alert('Second player won!')
      }
      // restarts game
      setProgress([0])
      const newGameSettings = Object.assign({}, gameSettings)
      newGameSettings.gameStarted = false
      setGameSettings(newGameSettings)
    }
  }, [progressArray, setProgress, gameSettings, setGameSettings, n])

  const clickOn = () => {
    // checks if player turn
    if(progressArray.length % 2 !== 0 || gameSettings.gameType !== 'AI') {
      if(progressArray[progressArray.length - 1] < m) {
        const newProgress = [...progressArray]
        newProgress[newProgress.length - 1]++
        setProgress(newProgress)
        // if limit reached adds new turn to progress array
        if(progressArray[progressArray.length - 1] === m - 1) {
          newProgress.push(0)
          setProgress(newProgress)
        }
      } 
    }
  }

  const changePlayer = () => {
    if(progressArray[progressArray.length - 1] !== 0) {
      const newProgress = [...progressArray, 0]
      setProgress(newProgress)
    }
  }

  const matches = useMemo(() => ( 
    Array(2 * n + 1 - progressArray.reduce( (a, b) => a + b)).fill('m')
  ), [progressArray, n])
  
  useEffect(() => setTimeout(checkWinner, 400), [checkWinner])
  useEffect(turnOfAI, [turnOfAI])

  return (
    <>
      <div className="matches">
        <p>{ matches.length }</p>
        { matches.map((match, index) => 
            <p alt="match" className="matchIMG" key={index} onClick={ clickOn }>&#x1F4CD;</p>) }
      </div>
      <button onClick={ changePlayer } className="button waves-effect waves-light btn-large">Okay</button>
    </>
  );
}

export default Matches;
