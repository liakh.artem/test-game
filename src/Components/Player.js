function Player({ player, playerN }) {
  const matchesImages = () => {
    const playerCount = player.reduce((a, b) => a + b),
      imgArr = []

    for(let i = 0; i < playerCount; i++) {
      imgArr.push(<p alt="match" key={ i } className="matchIMG">&#x1F4CD;</p>)
    }
    
    return imgArr.map(img => img)
  }

  return (
    <div className="col s6">
      <h4 className={ playerN === '1st' ? 'green' : 'red' }>{playerN} Player - { player.length > 0 ? player.reduce((a, b) => a + b) : '0' }</h4> 
      { player.length > 0 ? matchesImages() : '' }
    </div>
  );
}

export default Player;
