import React, {useState, useContext} from 'react'

const Context = React.createContext()

export const useMyContext = () => {
    return useContext(Context)
}

export const Provider = ({ children }) => {
    const [nm, setNM] = useState([12, 3]),
        [progressArray, setProgress] = useState([0]), // players last turns in a row
        [gameSettings, setGameSettings] = useState({
            gameMode: 'Normal', // first turn by AI/Player
            gameStarted: false, 
            gameType: 'AI' // vs AI/Player
        })

    return (
        <Context.Provider value={{
            gameSettings, setGameSettings,
            nm, setNM,
            progressArray, setProgress
            }}>
            {children}
        </Context.Provider>
    )
}
